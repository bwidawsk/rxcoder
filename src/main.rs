use anyhow::{anyhow};
use clap::Parser;
use isolang::Language;
use std::fs;
use std::io::{Error};
use std::path::PathBuf;
use std::process::Command;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[clap(
    after_long_help = "keep english audio and subs, re-encode audio, video\n\
        \t--english\n\
    re-encode nothing, but keep only english\n\
        \t--english --keep-video --keep-audio\n\
    Keep swedish audio, keep french subs, re-encode video\n\
        \t-s fra -a swe"
)]
struct DerivedArgs {
    /// Strip all subtitles
    #[clap(long)]
    strip_subs: bool,

    /// Subtitles to keep. All subtitles are kept if nothing is specified.
    #[clap(short, long, parse(try_from_str = parse_639), multiple_occurrences(true))]
    subs: Vec<String>,

    /// Audio tracks to keep. All tracks are kept if nothing is specified.
    #[clap(short, long, parse(try_from_str = parse_639), multiple_occurrences(true))]
    audio: Vec<String>,

    /// Simply stream copy the video
    #[clap(long)]
    keep_video: bool,

    /// Simply stream copy the audio
    #[clap(long)]
    keep_audio: bool,

    /// Equivalent to "--subs eng --audio eng"
    #[clap(long)]
    english: bool,

    /// CRF to use for video encoding
    #[clap(short, long, default_value_t = 25)]
    crf: usize,

    #[clap(short, long)]
    replace: bool,

    /// Input file
    #[clap(last = true, parse(from_os_str))]
    file: PathBuf,

    /// Scale to width
    #[clap(short = 'w', long)]
    scale_w: Option<usize>,

    /// Only dry run
    #[clap(short = 'n')]
    dry: bool,
}

fn parse_639(s: &str) -> Result<String, String> {
    if Language::from_639_3(s).is_some() {
        Ok(s.to_string())
    } else {
        Err(format!("Couldn't find language code {s}"))
    }
}

fn map_sub(cmd: &mut Command, lang: &String) {
    // stream index: 0
    // stream_type: s
    // stream specifier: m:language
    // '?' for optional
    cmd.args(["-map", format!("0:s:m:language:{lang}?").as_str()]);
}

fn map_aud(cmd: &mut Command, lang: &String) {
    // stream index: 0
    // stream_type: a
    // stream specifier: m:language
    // '?' for optional
    cmd.args(["-map", format!("0:a:m:language:{lang}?").as_str()]);
}

fn audio_encode(cmd: &mut Command) {
    cmd.args(["-c:a", "libfdk_aac"]);
    cmd.args(["-b:a", "192k"]);
    cmd.args(["-af", "loudnorm"]);
    cmd.args(["-ac", "2"]);
    cmd.args(["-ar", "48000"]);
}

fn video_encode(cmd: &mut Command, crf: usize) {
    cmd.args(["-c:v", "libx265"]);
    cmd.args(["-crf", &crf.to_string()]);
    cmd.args(["-preset", "slow"]);
    cmd.args(["-tag:v", "hvc1"]);
}

fn scale(cmd: &mut Command, width: usize) {
    cmd.args(["-vf", format!("scale={width}:-8").as_str()]);
}

/*
fn video_encode_hw((cmd: &mut Command) {
    // Find first render node
    let files = match fs::read_dir("/dev/dri/") {
        Ok(iter) => iter,
        Err(_) => panic!("Couldn't find a render node"),
    };

    cmd.args(["-init_hw_device", "vaapi=foo:/dev/dri/renderD128"})
}
*/

fn name_output() -> String {
    "foo.mkv".to_string()
}

fn main() -> anyhow::Result<()> {
    let args = DerivedArgs::parse();
    let mut subs_mod = args.subs.to_vec();
    let mut audio_mod = args.audio.to_vec();

    let (english, subs, auds) = (args.english, &args.subs, &args.audio);
    match (english, subs, auds) {
        (true, y, z) if !(y.is_empty() && z.is_empty()) => panic!(),
        (true, _, _) => {
            subs_mod = vec!["eng".to_string()];
            audio_mod = vec!["eng".to_string()]
        }
        (_, _, _) => (),
    };
    let mut xcode = Command::new("/usr/bin/ffmpeg");

    xcode.arg("-hide_banner");

    let input = args
        .file
        .into_os_string()
        .into_string()
        .expect("Couldn't convert input file to string");

    xcode.args(["-i", &input]);

    // Always keep the metadata
    xcode.args(["-movflags", "use_metadata_tags"]);

    xcode.args(["-threads", "0"]);

    // Select all streams
    xcode.args(["-map", "0"]);

    // Default to copying everything. Below will remove/re-encode
    xcode.args(["-c", "copy"]);

    if !subs_mod.is_empty() || args.strip_subs {
        // Negative mapping to omit all other subtitle streams
        xcode.args(["-map", "-0:s"]);

        // Keep the subs we want
        subs_mod.iter().for_each(|x| map_sub(&mut xcode, x));
    }

    // Negative mapping to omit all other audio streams
    if !audio_mod.is_empty() {
        xcode.args(["-map", "-0:a"]);

        // Keep the audio we want
        audio_mod.iter().for_each(|x| map_aud(&mut xcode, x));
    }

    if !args.keep_video {
        video_encode(&mut xcode, args.crf);
    }

    if let Some(width) = args.scale_w {
        scale(&mut xcode, width);
    }

    if !args.keep_audio {
        audio_encode(&mut xcode);
    }

    let out_file = name_output();
    xcode.arg(&out_file);

    if args.dry {
        println!("{xcode:?}");
        return Ok(());
    }

    match xcode
        .spawn()
        .expect("Couldn't start ffmpeg")
        .wait()
        .expect("Failed to wait on child")
        .code()
    {
        Some(code) => {
            if code != 0 {
                return Err(anyhow!("Failed: {}", Error::from_raw_os_error(code)));
            }
            if args.replace {
                fs::rename(&out_file, &input)?;
            }
        }
        None => println!("Terminated by signal"),
    };

    Ok(())
}
